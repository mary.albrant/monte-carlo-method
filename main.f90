program monteCarlo_square_circle
  implicit none
  
  integer :: N, k, i
  real(8) :: radius, s_circle, s_quadra
  real(8), dimension(:) :: center(2)
  real(8), allocatable, dimension(:,:) :: rand_array
  
  open(1, file = "input")
  read(1,*) center, radius, N
  write(*,*) "N :", N
  
  allocate(rand_array(N,2))
  call random_seed
  call random_number(rand_array)
  
  rand_array(:,1) = (center(1) - radius)*rand_array(:,1) + (center(1) + radius)*(1 - rand_array(:,1))
  rand_array(:,2) = (center(2) - radius)*rand_array(:,2) + (center(2) + radius)*(1 - rand_array(:,2))
  
  k = 0
  do i = 1, N
     if (sqrt(dot_product(rand_array(i,:) - center,rand_array(i,:) - center)) < radius) then
         k = k + 1 
     endif
  enddo
  s_quadra = (2.0*radius)**2
  s_circle = s_quadra * k / N

  write(*,*) "circle square : ", 4.0*atan(1.0)*(radius**2)
  write(*,*) "circle square by Monte Carlo : ", s_circle
  
  close(1)

end program
