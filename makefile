my=myprog
files=$(wildcard *.f90)
obj=$(patsubst %.f90, %.o, $(files))

$(my): $(obj)                                                          
		gfortran $^ -o $@
%.o: %.f90
		gfortran -c $<

main.o: main.f90
		gfortran -c $<
clean:
		rm -f $(my) *.o *.mod
run: $(my)
		./$(my)
.PHONY: clean run
